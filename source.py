import os
import pandas as pd
import random
from flask import (
    Flask,
    render_template,
    request,
    redirect,
    url_for,
    abort,
    send_from_directory,
)
from werkzeug.utils import secure_filename

app = Flask(__name__)
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLD = "uploads"
UPLOAD_FOLDER = os.path.join(APP_ROOT, UPLOAD_FOLD)
app.config["UPLOAD_EXTENSIONS"] = [".csv", ".pdf", ".gif"]
app.config["UPLOAD_PATH"] = UPLOAD_FOLDER
namess = list()


def convert_file(df):
    index_ = random.randint(1, 100000)
    agg_functions = {
        "to": "first",
        "amount": "sum",
        "token": "first",
        "contract": "first",
        "memo": "first",
    }
    df_new = df.groupby(["memo", "to"]).aggregate(agg_functions)
    df_new["amount"] = df_new["amount"].round(2)
    df_new = df_new.reset_index(drop=True)
    df_new = df_new.astype(object)
    file_path = os.path.join(
        app.config["UPLOAD_PATH"], f"{namess[0]}_{namess[len(namess) - 1]}.csv"
    )
    df_new.to_csv(file_path, index=False)
    return f"{namess[0]}_{namess[len(namess) - 1]}.csv"


@app.route("/")
def index():
    files = os.listdir(app.config["UPLOAD_PATH"])
    return render_template("index.html", files=files)


@app.route("/", methods=["POST"])
def upload_files():
    files = request.files.getlist("file")

    dfs = list()
    for uploaded_file in files:
        filename = secure_filename(uploaded_file.filename)
        if filename != "":
            new_ff = filename.replace(".csv", "")
            namess.append(new_ff)
            path = os.path.join(app.config["UPLOAD_PATH"], filename)
            uploaded_file.save(path)
            dfs.append(pd.read_csv(path))
            os.remove(path)
    big_frame = pd.concat(dfs, ignore_index=True)
    n = convert_file(big_frame)

    return send_from_directory(app.config["UPLOAD_PATH"], n)


@app.route("/uploads/<filename>")
def upload(filename):
    return send_from_directory(app.config["UPLOAD_PATH"], filename)
